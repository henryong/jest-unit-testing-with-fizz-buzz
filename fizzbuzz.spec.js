const say = require('./fizzbuzz')

//! NOTE:
//* toEqual() checks equivalence. 
//* toBe(), on the other hand, makes sure that they're the exact same object.
it('Get Fizz', () => {
  expect(say(3)).toEqual('Fizz')
});

it('Get Buzz', () => {
  expect(say(5)).toEqual('Buzz')
});

it('Get Fizz Buzz', () => {
  expect(say(15)).toEqual('Fizz Buzz')
});

it('Get numeric output instead if no Fizz and/or Buzz', () => {
  expect(say(1544)).toEqual(1544)
});

//! NOTE:
//* Better approach is to iterate thru list of possible conditions.
describe.each`
  input  | expected
  ${3}   | ${'Fizz'}
  ${5}   | ${'Buzz'}
  ${15}  | ${'Fizz Buzz'}
  ${154} | ${154}
`('Test FizzBuzz say() function on different inputs.', ({ input, expected }) => {
  it(`returns ${expected}`, () => {
    expect(say(input)).toEqual(expected);
  });
});
