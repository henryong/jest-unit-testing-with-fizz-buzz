//* Fizz Buzz CONDITIONS:
//* 1. Inputs a Integer. Outputs a String.
//* 2. Factors of 3 gives Fizz.
//* 3. Factors of 5 returns Buzz.
//* 4. Factors of 15 returns Fizz Buzz.
//* 5. Otherwise All other numbers returns the number itself.

//TODO: Naive Solution:
//? Modules 15 -> Yes, return Fizz Buzz
//? else...
//? Modules 5  -> Yes, return Buzz
//? else...
//? Modules 3  -> Yes, return Fizz
//? else...
//? Default -> return number

function say (numeric) {
  let word = ''
  if (numeric % 3 === 0) word += 'Fizz'
  if (numeric % 5 === 0) word += ' Buzz'
  return word.trim() || numeric
}

module.exports = say
